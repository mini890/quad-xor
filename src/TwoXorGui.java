import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class TwoXorGui extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private Xor xorAlice, xorBernard;
	
	private JTextField aliceKeyText, bernardKeyText;
	private char[] aliceLongKey, bernardLongKey;
	private JTextField clearTextIn;
	
	private char[] msgChar = new char[0];
	private char[] firstMsgEncoded, secondMsgEncoded, thirdMsgEncoded;
	private CharAndBits[] msg1XORmsg2, msg12XORmsg3;
	private char[] msgDecoded;
	
	private JTable table;
	private MyModel myModel;
	private TableColumnModel colModel;
	
	private JPanel centerPanel;
	
	private static final String XOR = "   XOR ^";
	
	private static final String[] firstCol = {"", "Alice Orig Msg", "Alice Key", "Encrypts", XOR, " ", "Bernard", "Bernard Key", "Encrypts", XOR, " ", "Alice", "Alice Key", "Decrypts", XOR, " ", "Bernard", "Bernard Key", "Decrypts", XOR, " ", "Eve's 1st catch", "Eve's 2nd catch", " ", " ", "Eve's 3rd catch", " ", "Orig Msg ?"};
	private static final int ASCII = 0, ALICE1 = 1, ALICE_KEY1 = 2, SPACE1 = 3, XOR1 = 4, SPACE2 = 5, BERNARD1 = 6, BERNARD_KEY1 = 7, SPACE3 = 8, XOR2 = 9, SPACE4 = 10, ALICE2 = 11, ALICE_KEY2 = 12, SPACE5 = 13, XOR3 = 14, SPACE6 = 15, BERNARD2 = 16, BERNARD_KEY2 = 17, SPACE7 = 18, XOR4 = 19, SPACE8 = 20, LF1 = 21, LF2 = 22, LF3 = 23, LF4 = 24, LF5 = 25, LF6 = 26, ORIGBACK = 27;
	
	TwoXorGui() {
		super("XOR encoding/decoding TWICE by Mini890");
		setLayout(new BorderLayout());
		
		xorAlice = new Xor("");
		xorBernard = new Xor("");
		aliceLongKey = xorAlice.dupKey(0).toCharArray();
		bernardLongKey = xorBernard.dupKey(0).toCharArray();
		DocumentListener dc = new KeyListener();
		JPanel north = new JPanel (new GridLayout(7, 1, 2, 2));
		north.add(createCenteredLabel("Enter Alice's Key"));
		aliceKeyText = new JTextField(50);
		aliceKeyText.getDocument().addDocumentListener(dc);
		north.add(aliceKeyText);
		// the key of Bernard
		north.add(createCenteredLabel("Enter Bernard's Key"));
		bernardKeyText = new JTextField(50);
		bernardKeyText.getDocument().addDocumentListener(dc);
		north.add(bernardKeyText);
		
		// the message
		north.add(createCenteredLabel("Enter Alice's message here"));
		clearTextIn = new JTextField(50);
		clearTextIn.getDocument().addDocumentListener(new ClearListener());
		north.add(clearTextIn);
		// a gap
		north.add(new JLabel(" "));
		// add this panel to the top of the screen
		add(north, BorderLayout.NORTH);

		// in the CENTER region of the frame we will put a JTable containing all the 
		// encrypted/decripted bits
		myModel = new MyModel();
		table = new JTable(myModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		colModel = table.getColumnModel();

		// the centerPanel will receive the JTable in a scrollPane
		centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(new JScrollPane(table), BorderLayout.CENTER);
		add(centerPanel, BorderLayout.CENTER);

		// standard operation to show the JFrame
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(30, 30, 900, 700);
		setVisible(true);

		// to set the size of column 1
		updateStringToEncode();
	}

	/**
	 * A method to create a JLabel with foreground color Blue and with text centered
	 */
	private JLabel createCenteredLabel(String text) {
		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.BLUE);
		return label;
	}

	/**
	 * One of the key has changed we could have two separated methods for
	 * each key but we will have to encode/decode all the messages in the path anyway 
	 */
	private void updateKeyString() {
		// update key in the Xor object from the key JTextField
		xorAlice.setKey(aliceKeyText.getText());
		xorBernard.setKey(bernardKeyText.getText());
		// update the encoding process
		updateStringToEncode();
	}

	/**
	 * To update the string to be coded
	 */
	private void updateStringToEncode() {
		// get the text of the message to encode from the JTextField
		String line = clearTextIn.getText();
		// make the char[] array out of it to be displayed in the JTable
		msgChar = line.toCharArray();
		// generate (for display purpose only) the key that will be used for every character
		aliceLongKey = xorAlice.dupKey(line.length()).toCharArray();
		bernardLongKey = xorBernard.dupKey(line.length()).toCharArray();
		// build the first encrypted message with Alice's key
		String encoded = xorAlice.encodeDecode(line);
		// in a char[] for display purpose
		firstMsgEncoded = encoded.toCharArray();
		// prepare the XOR of the messages intercepted by Eve
		CharAndBits[] msg1 = CharAndBits.newCharAndBitsArray(encoded);
		// build the encrypted messaage with Bernard's key
		encoded = xorBernard.encodeDecode(encoded);
		// in a char[]
		secondMsgEncoded = encoded.toCharArray();
		// prepare the XOR of the messages intercepted by Eve
		CharAndBits[] msg2 = CharAndBits.newCharAndBitsArray(encoded);
		// third translation Alice removes her key
		encoded = xorAlice.encodeDecode(encoded);
		// in a char[]
		thirdMsgEncoded = encoded.toCharArray();
		// prepare the XOR of the messages intercepted by Eve
		CharAndBits[] msg3 = CharAndBits.newCharAndBitsArray(encoded);
		// finally Bernard decoded the message
		encoded = xorBernard.encodeDecode(encoded);
		// in char[]
		msgDecoded = encoded.toCharArray();
		
		// Eve can now XOR the whole messages intercepted
		String tmp = CharAndBits.xorArray(msg1, msg2);
		msg1XORmsg2 = CharAndBits.newCharAndBitsArray(tmp);
		tmp = CharAndBits.xorArray(msg1XORmsg2, msg3);
		msg12XORmsg3 = CharAndBits.newCharAndBitsArray(tmp);
		
		// inform the model that the table contain changed 
		myModel.fireTableStructureChanged();
		myModel.fireTableDataChanged();
		// set the size of the column when a new column is added lets set it's size
		int actualColumnCount = (msgChar.length * 2) + 1;
		// no need to perform the columns width if one of the key is not defined
		if(aliceLongKey.length == 0 || bernardLongKey.length == 0)
			return;

		// set columns width
		for(int i = 0; i < actualColumnCount; i++) {
			TableColumn tc = colModel.getColumn(i); 
			tc.setPreferredWidth(100);
			tc.setMinWidth(100);
		}
	}

	/**
	 * To start the GUI
	 */
	public static void main(String[] args) {
		new TwoXorGui();
	}

	/**
	 * A listener to be informed whenever the JTextField of the clear text is changed
	 */
	private class ClearListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
	}

	/**
	 * A listener to be informed whenever the JTextField of the SWAP or ROTATE key is changed
	 */
	private class KeyListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
	}

	/** 
	 * A class that extends AbstractTableModel to povide the binary representation
	 * of every cell of the JTable in the center panel
	 */
	private class MyModel extends AbstractTableModel {

		private static final long serialVersionUID = 1L;
		// for ----- under the XOR
		private String dash = "--------------";
		// the String showing the message is transmitted
        private String arrow = "< received";
        
		// the number of columns is the length of the message + the first column
		public int getColumnCount() {
			// if any of the key is empty just show the first column
			if(aliceLongKey.length == 0 || bernardLongKey.length == 0)
				return 1;
			// else 2 columns for each letter of the message + the header column
			return  (msgChar.length * 2) + 1; 
		}

		// name of each colum (first one is empty)
		public String getColumnName(int column) {
			// if column 0 we return the hardcode "Steps"
			if(column == 0)
				return "Steps";
			
			// skip first column which is a header
			--column;
			
			// now for each char is the msg we have 2 column
			if(column / 2 >= msgChar.length)
				return "";

			// OK generate the title for the left coplumn or "transmitted" for the second
			if(column % 2 == 0)
				return "Digit #" + ((column / 2) + 1);
			else
				return "Transmitted";
		}

		// return the row count
		public int getRowCount() {
			// it is the length of our first column
			return firstCol.length;
		}

		// the JTable want's to know what to print there
		public Object getValueAt(int row, int col) {
			// for the first column we just return the header
			if(col == 0)
				return firstCol[row];
			// if no keys are entered no need to continue
			if(aliceLongKey.length == 0 || bernardLongKey.length == 0)
				return "";
			
			// get rid of the first column which shows headers
			--col;
			// validate first the col it should be contained in the mesage
			if((col / 2) >= msgChar.length)
				return "";
			
			// the CharAndBits to display init to null we will check it later
			CharAndBits cab = null;

			// check if left or right column
			boolean left = col % 2 == 0;
			// index in the messages
			col /= 2;
	
			// depending of the column display the correct text field
			switch(row) {
			    // first row the charac of the message and the 2 keys
				case ASCII:
					if(left)
						return "   Char: " + msgChar[col];
					else
						return " Keys: " + aliceLongKey[col] + "-" + bernardLongKey[col];
						
				// the encoded message from Alice
				case ALICE1:
					// the right column contains nothing
					if(!left)
						return "";
				    // left column the corresponding char in the original message
					cab = new CharAndBits(msgChar[col]);
					break;
					
	            // the binary representation of the Alice's key (same one at both row)
				case ALICE_KEY1:
				case ALICE_KEY2:
					// the right column
					if(!left)
						return "";
					// the left column
					cab = new CharAndBits(aliceLongKey[col]);
					break;		
									
				// Bernard first field
				case BERNARD1:
					// in the right column the String saying it received hat message
					if(!left)
						return arrow;
				case XOR1:				// which is Alice first encoding
					cab = new CharAndBits(firstMsgEncoded[col]);
					break;
					
				// Alice's second decoding where she removes her key
				case ALICE2:
					// right column show the message is received
					if(!left)
						return arrow;
				case XOR2:				// which is Alice second operation
					cab = new CharAndBits(secondMsgEncoded[col]);
					break;
		
				// Bernard second intervention. He decrypts the message
				case BERNARD2:
					// right column shows he received the message from Alice
					if(!left)
						return arrow;
				case XOR3:				// which is the 3rd message transmitted
					cab = new CharAndBits(thirdMsgEncoded[col]);
					break;
		
				case XOR4:				// final decode by Bernard
					if(!left)
						return " <--- Orig msg ?";
					cab = new CharAndBits(msgDecoded[col]);
					break;
					
					
	            // the binary representation of the Bernard's key (same one at both row)
				case BERNARD_KEY1:
				case BERNARD_KEY2:
					// the right column
					if(!left)
						return "";
					// the left column
					cab = new CharAndBits(bernardLongKey[col]);
					break;		
				
				// the space between each cluster
				case SPACE2:
				case SPACE4:
				case SPACE6:
				case SPACE8:
					return "";
					
				// the space before of the --------------
				case SPACE3:
					if(left)
						return dash;
					return " sent to Alice";
				case SPACE7:
					if(left)
						return dash;
					return "";
					
				case SPACE1:
				case SPACE5:
					if(left)
						return dash;
					return " sent to Bernard";
					
				// XOR of the 3 transmitted messages
				case LF1:
					if(left)
						return "  Msg #1";
					cab = new CharAndBits(firstMsgEncoded[col]);
					break;
				case LF2:
					if(left)
						return "  Msg #2";
					cab = new CharAndBits(secondMsgEncoded[col]);
					break;
				case LF3:
					if(left)
						return "";
					return dash;
				case LF4:
					if(left)
						return XOR;
					// XOR of the 2 first MSG
					cab = msg1XORmsg2[col];
					break;
				case LF5:
					if(left)
						return "  Msg #3";
					cab = new CharAndBits(thirdMsgEncoded[col]);
					break;
				case LF6:
					if(left)
						return "";
					return dash;
				case ORIGBACK:
					if(left)
						return XOR;
					//  MSG
					cab = msg12XORmsg3[col];
					break;
					
			}
			// common formatting the binary string followed by the printable version of the char
			// unless not defined yet due to the way the GUI refresh the JTable
			if(cab == null)
				return "";
			return cab.toBinaryString();
		}

	}

}