public class TwoXor {
	
	public static void main(String[] args) {
		 
		String msg = "DreamInCode";
		String aliceKey = "Macosxnerd101";
		String bernardKey = "Dogstopper";
		
		Xor xorAlice = new Xor(aliceKey);
		Xor xorBernard = new Xor(bernardKey);
		String aliceDupKey = xorAlice.dupKey(msg.length());
		String bernardDupKey = xorBernard.dupKey(msg.length());
		String aliceKeyInBin = CharAndBits.toBinaryString(aliceDupKey);
		String bernardKeyInBin = CharAndBits.toBinaryString(bernardDupKey);
		char[] dash = new char[aliceKeyInBin.length()];
		for (int i = 0; i < aliceKeyInBin.length(); i++)
			dash[i] = '-';
		String dashStr = new String(dash);
		
		System.out.println("The original message is: \"" + msg + "\" the key used will be \"" + aliceDupKey + "\"");
		String msgInBin = CharAndBits.toBinaryString(msg);
		System.out.println(msgInBin);
		System.out.println(aliceKeyInBin);
		System.out.println(dashStr);
		
		String encoded = xorAlice.encodeDecode(msg);
		String encodedInBinary = CharAndBits.toBinaryString(encoded);
		System.out.println(encodedInBinary);
		
		System.out.println();
		
		System.out.println("The first encoded message from Alice XORed with Bernard's key which is \"" + bernardDupKey + "\"");
		System.out.println(encodedInBinary);
		System.out.println(bernardKeyInBin);
		System.out.println(dashStr);
		
		String decoded = xorBernard.encodeDecode(encoded);
		encodedInBinary = CharAndBits.toBinaryString(decoded);
		System.out.println(encodedInBinary);
		System.out.println();
		
		System.out.println("The encoded message from Bernard to which we XOR Alice's key wich is \"" + aliceDupKey + "\"");
		System.out.println(encodedInBinary);
		System.out.println(aliceKeyInBin);
		System.out.println(dashStr);
		decoded = xorAlice.encodeDecode(decoded);
		encodedInBinary = CharAndBits.toBinaryString(decoded);
		System.out.println(encodedInBinary);
		System.out.println();
		
		System.out.println("Finaly Bernard applying again his key \"" + bernardDupKey + "\"");
		System.out.println(encodedInBinary);
		System.out.println(bernardKeyInBin);
		System.out.println(dashStr);
		
		decoded = xorBernard.encodeDecode(decoded);
		encodedInBinary = CharAndBits.toBinaryString(decoded);
		System.out.println(encodedInBinary);
		System.out.println("The final decoded message is \"" + decoded + "\" is it the same as \"" + msg + "\": " + msg.equals(decoded));
	}

}